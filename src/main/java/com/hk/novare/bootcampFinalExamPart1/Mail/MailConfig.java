package com.hk.novare.bootcampFinalExamPart1.Mail;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class MailConfig {

        @Value("${spring.mail.host}")
        private String mailServerHost;

        @Value("${spring.mail.port}")
        private int mailServerPort;

        @Value("${spring.mail.username}")
        private String mailServerUserName;

        @Value("${spring.mail.password}")
        private String mailServerPassword;

        @Value("${spring.mail.properties.mail.smtp.auth}")
        private String mailServerAuth;

        @Value("${spring.mail.properties.mail.starttls.enable}")
        private String mailServerTls;

        @Bean
        public JavaMailSender javaMailSender(){

            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

            mailSender.setPort(mailServerPort);
            mailSender.setHost(mailServerHost);
            mailSender.setUsername(mailServerUserName);
            mailSender.setPassword(mailServerPassword);

            Properties props = mailSender.getJavaMailProperties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.auth", mailServerAuth);
            props.put("mail.smtp.starttls.enable", mailServerTls);
            props.put("mail.debug", "true");

            return mailSender;
        }
    }


