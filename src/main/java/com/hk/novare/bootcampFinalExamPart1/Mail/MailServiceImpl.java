package com.hk.novare.bootcampFinalExamPart1.Mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


@Service
public class MailServiceImpl implements MailService{

    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public void sendHtmlMessage(String to, String subject, String htmlMessage) {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        try{

            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

            mimeMessageHelper.setTo(InternetAddress.parse(String.valueOf(to)));
            mimeMessageHelper.setSubject(subject);
            mimeMessageHelper.setText(htmlMessage, true);

            javaMailSender.send(mimeMessage);

        }catch (MessagingException e){
            e.printStackTrace();
        }

    }
}
