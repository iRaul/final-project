package com.hk.novare.bootcampFinalExamPart1.Mail;

public interface MailService {
    void sendHtmlMessage(String to, String subject, String htmlMessage);
}
