package com.hk.novare.bootcampFinalExamPart1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootcampFinalExamPart1Application
{
	public static void main(String[] args)
	{
		SpringApplication.run(BootcampFinalExamPart1Application.class, args);
	}
}
