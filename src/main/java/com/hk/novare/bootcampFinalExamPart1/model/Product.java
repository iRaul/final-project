package com.hk.novare.bootcampFinalExamPart1.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "product")
public class Product {

    //Product Model attributes
    @Id
    //@TableGenerator(name = "EntityIdGen", table = "TABLE_GENERATOR", pkColumnValue = "Entity")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "product_name")
    String productName;

    @Column(name = "category")
    String category;

    @Column(name = "brand")
    String brand;

    @Column(name ="product_description")
    String productDescription;

    @Column(name ="image_path")
    String imagePath;

    @Column(name = "product_quantity")
    int productQuantity;

    @Column(name = "price")
    double price;
}