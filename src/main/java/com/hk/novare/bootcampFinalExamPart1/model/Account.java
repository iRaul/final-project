package com.hk.novare.bootcampFinalExamPart1.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "account")
public class Account {

	//account table attributes
	@Id
	//@TableGenerator(name = "EntityIdGen", table = "TABLE_GENERATOR", pkColumnValue = "Entity")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	Long id;

	@Column(name = "account_name")
	String accountName;

	@Column(name = "account_number")
	String accountNumber;

	@Column(name = "account_type")
	String accountType;

	@Column(name = "balance")
	double balance;

	@Column(name ="expiration_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	Instant expirationDate;

	//reference mapping from order connection
	//@OneToOne(mappedBy = "account")
	//private Order order;

	// many to one relationship of user to account
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", insertable = true)
	private User user;


	//not sure if needed
//  @Column(name = "expenses")
//  double expenses;
}
