package com.hk.novare.bootcampFinalExamPart1.Model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "transaction")
public class Transaction {

    //transaction model attributes
    @Id
    //@TableGenerator(name = "EntityIdGen", table = "TABLE_GENERATOR", pkColumnValue = "Entity")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "payment_option")
    String paymentOption;

    // one to one connection from order model
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "order_id", referencedColumnName = "id",insertable = true )
    private Order order;


}
