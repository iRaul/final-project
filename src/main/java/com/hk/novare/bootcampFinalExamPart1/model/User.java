package com.hk.novare.bootcampFinalExamPart1.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "user")
public class User
{
	// user table attributes
	@Id
	//@TableGenerator(name = "EntityIdGen", table = "TABLE_GENERATOR", pkColumnValue = "Entity")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	Long id;

	@Column(name = "username")
	String username;

	@Column(name = "name")
	String name;

	@Column(name ="password")
	String password;

	@Column(name ="type")
	String type;

	@Column(name = "email")
	String email;

	@Column(name = "address")
	String address;

	@Column(name = "mobile_number")
	String mobileNumber;
}
