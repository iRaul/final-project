package com.hk.novare.bootcampFinalExamPart1.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.Instant;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "cart")
public class Cart
{
	//cart table attributes
	@Id
	//@TableGenerator(name = "EntityIdGen", table = "TABLE_GENERATOR", pkColumnValue = "Entity")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	Long id;

	@Column(name = "order_quantity")
	int orderQuantity;

	@ManyToOne(cascade = CascadeType.PERSIST)
	//@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "product_id", referencedColumnName = "id",insertable = true )
	public Product product;

	@ManyToOne(cascade = CascadeType.PERSIST)
	//@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "order_id", referencedColumnName = "id",insertable = true )
	public Order order;
}
