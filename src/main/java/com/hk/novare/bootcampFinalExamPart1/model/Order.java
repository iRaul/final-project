package com.hk.novare.bootcampFinalExamPart1.Model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "order")
public class Order {

	@Id
	//@TableGenerator(name = "EntityIdGen", table = "TABLE_GENERATOR", pkColumnValue = "Entity")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	Long id;

	@Column(name = "payment_option")
	String paymentOption;

	@Column(name = "order_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	Instant orderDate;

	@Column(name = "total")
	double total;

	@Column(name = "status")
	String status;

	//Active, Inactive, Sold, Unpaid.

	//one to one connection to user model
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", referencedColumnName = "id",insertable = true )
	private User user;

	// one to one connection from account model
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "account_id", referencedColumnName = "id",insertable = true )
	private Account account;

	// one to one connection from cart model
	//@ManyToOne(cascade = CascadeType.ALL)
	//@JoinColumn(name = "cart_id", referencedColumnName = "id",insertable = true )
	//private Cart cart;
}