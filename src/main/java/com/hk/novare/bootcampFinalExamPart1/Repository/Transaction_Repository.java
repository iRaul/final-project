package com.hk.novare.bootcampFinalExamPart1.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hk.novare.bootcampFinalExamPart1.Model.Transaction;

@Repository
public interface Transaction_Repository extends JpaRepository<Transaction, Long>
{
	
}