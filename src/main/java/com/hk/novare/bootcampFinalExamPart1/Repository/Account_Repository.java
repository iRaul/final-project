package com.hk.novare.bootcampFinalExamPart1.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hk.novare.bootcampFinalExamPart1.Model.Account;

@Repository
public interface Account_Repository extends JpaRepository<Account, Long>
{
	
}