package com.hk.novare.bootcampFinalExamPart1.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hk.novare.bootcampFinalExamPart1.Model.User;

@Repository
public interface User_Repository extends JpaRepository<User, Long>
{
	User findUserByUsernameAndPassword(String set_Username, String set_Password);
}