package com.hk.novare.bootcampFinalExamPart1.Repository;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hk.novare.bootcampFinalExamPart1.Model.Cart;
import com.hk.novare.bootcampFinalExamPart1.Model.Order;
import com.hk.novare.bootcampFinalExamPart1.Model.Product;

@Repository
public interface Cart_Repository extends JpaRepository<Cart, Long>
{
	public <T> Set<T> findAllByOrder(Order set_Order, Class<T> set_Type);
	public Set<Cart> findAllByOrder(Order set_Order);
	public Cart findByProductIdAndOrderId(Long set_Order_Id, Long set_Product_Id);
	//public void deleteByOrderIdAndProductIds(Long set_Cart_Id, List<Integer> set_Product_Id);
	//public Set<Cart> findAllByOrder(Order set_Order);
	//public void deleteByCart(Cart set_Cart);
	//public void deleteAll(List<Cart> set_Cart);
	//public void deleteAll();
	public void deleteAllByOrderIdAndProductIdIn(Long set_Order, List<Long> set_Product);
	public void deleteAllByIdIn(List<Long> set_Cart);
	public void deleteByIdIn(List<Long> set_Cart);

	//@Query(value = )
}