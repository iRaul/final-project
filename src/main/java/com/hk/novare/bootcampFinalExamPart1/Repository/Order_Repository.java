package com.hk.novare.bootcampFinalExamPart1.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

import com.hk.novare.bootcampFinalExamPart1.Model.Order;
import com.hk.novare.bootcampFinalExamPart1.Model.User;

@Repository
public interface Order_Repository extends JpaRepository<Order, Long>
{
	public Order findByUserAndStatus(User set_User, String set_Status);
	public Set<Order> findAllByUserIdAndStatus(Long set_UserId, String set_Status);
}