package com.hk.novare.bootcampFinalExamPart1.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hk.novare.bootcampFinalExamPart1.Model.Token;

@Repository
public interface Token_Repository extends JpaRepository<Token, Long>
{
	
}