package com.hk.novare.bootcampFinalExamPart1.Repository;

import java.util.List;
import java.lang.Iterable;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hk.novare.bootcampFinalExamPart1.Model.Product;

@Repository
public interface Product_Repository extends JpaRepository<Product, Long>
{
	Page<Product> findAllByCategory(String set_Category, Pageable set_Pageable);
	Product findOneById(Long set_Id);
	Set<Product> findByProductNameContaining(String set_ProductName);
	//Iterable<Product> findAll(Sort sort);
	//Page<Product> findAll(Pageable pageable);
}