package com.hk.novare.bootcampFinalExamPart1.Controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;

import com.hk.novare.bootcampFinalExamPart1.Model.User;
import com.hk.novare.bootcampFinalExamPart1.Service.User_Service;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("api/1.0.0/User")
public class User_Controller
{
	@Autowired
	private User_Service _User_Service_;

	@PostMapping(value = "Login")
	public ResponseEntity Login(@RequestBody Map<String, Object> set_Data)
	{
		return ResponseEntity.ok(this._User_Service_.Login(set_Data));
	}

	@GetMapping(value = "Logout")  
	public ResponseEntity Logout(HttpServletRequest set_HttpServletRequest, HttpServletResponse set_HttpServletResponse)
	{
		return ResponseEntity.ok(this._User_Service_.Logout(set_HttpServletRequest, set_HttpServletResponse));
	}

	@PostMapping(value = "Customer/Register")
	public ResponseEntity Customer_Register(@RequestBody User set_User)
	{
		return ResponseEntity.ok(_User_Service_.Insert_Customer(set_User));
	}
}