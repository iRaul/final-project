package com.hk.novare.bootcampFinalExamPart1.Controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.hk.novare.bootcampFinalExamPart1.Model.Product;
import com.hk.novare.bootcampFinalExamPart1.Service.Product_Service;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("api/1.0.0/Product")
public class Product_Controller
{
	@Autowired
	private Product_Service _Product_Service_;

	@GetMapping(value = "qwe")
	public ResponseEntity qwe()
	{
		return ResponseEntity.ok(_Product_Service_.paginggg());
	}

	@GetMapping(value = "{Id}")
	public ResponseEntity Get_Specific(@PathVariable("Id") Long set_Id)
	{
		return ResponseEntity.ok(this._Product_Service_.Get_Specific(set_Id));
	}

	@GetMapping(value = "All")
	public ResponseEntity Get_All()
	{
		return ResponseEntity.ok(this._Product_Service_.Get_All());
	}

	@GetMapping(value = "{Action}/{PageNumber}")
	public ResponseEntity Get_All(@PathVariable("Action") String set_Action, @PathVariable("PageNumber") Integer set_PageNumber)
	{
		return ResponseEntity.ok(this._Product_Service_.Get_All(set_Action, set_PageNumber));
	}

	@GetMapping(value = "{Action}/{Data}/{PageNumber}")
	public ResponseEntity Get_All_By_Category(@PathVariable("Action") String set_Action, @PathVariable("Data") String set_Data, @PathVariable("PageNumber") Integer set_PageNumber)
	{
		return ResponseEntity.ok(this._Product_Service_.Get_All_By_Category(set_Action, set_Data, set_PageNumber));
	}

	//consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE
	@PostMapping(value = "Add")
	public ResponseEntity Add_Product(@RequestBody Product set_Product)
	{
		return ResponseEntity.ok(this._Product_Service_.Insert(set_Product));
	}

	@PostMapping(value = "Delete/{Id}")
	public ResponseEntity Delete(@PathVariable("Id") Long set_Id)
	{
		return ResponseEntity.ok(this._Product_Service_.Delete(set_Id));
	}

	@GetMapping(value = "Search")
	public ResponseEntity Search_Product(@RequestParam String name)
	{
		return ResponseEntity.ok(this._Product_Service_.Search_Product(name));
	}
}	