package com.hk.novare.bootcampFinalExamPart1.Controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;

import com.hk.novare.bootcampFinalExamPart1.Model.Order;
import com.hk.novare.bootcampFinalExamPart1.Service.Order_Service;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("api/1.0.0/Order")
public class Order_Controller
{
	@Autowired
	private Order_Service _Order_Service_;

	@PostMapping()
	public ResponseEntity haysssXD(@RequestBody Map<String, Object> set_Data)
	{
		return ResponseEntity.ok(this._Order_Service_.haysssXD(set_Data));
	}
}