package com.hk.novare.bootcampFinalExamPart1.Controller;

import com.hk.novare.bootcampFinalExamPart1.Service.Token_Service;
import com.hk.novare.bootcampFinalExamPart1.Model.Account;
import com.hk.novare.bootcampFinalExamPart1.Model.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Optional;

@RestController
@RequestMapping("api/1.0.0/Token")
public class Token_Controller
{

	@Autowired
	Token_Service _Token_Service_;

	@GetMapping(value = "/Get/All")
	public ResponseEntity Get_All_Token()
	{
		return ResponseEntity.ok(this._Token_Service_.Get_Token());
	}

	@GetMapping(value = "/Get/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public Optional<Token> Get_Account_By_Id(@RequestParam Long id)
	{ return _Token_Service_.Get_Token_By_Id(id); }

	@DeleteMapping("/Delete/{id}")
	public void Delete_Token_By_Id(@PathVariable(value="id") Long id){
		_Token_Service_.Delete_Token_Id(id);
	}

	@PostMapping(value = "/Add")
	public ResponseEntity Add_Product(@RequestBody HashMap<String, Object> set_Data)
	{ return ResponseEntity.ok(this._Token_Service_.Add_Token(set_Data)); }

	@PutMapping("/Update/{id}")
	public void updateBooks(@RequestBody Token token,@PathVariable("id") Long id){
		_Token_Service_.Update_By_Id(token, id);
	}
}
