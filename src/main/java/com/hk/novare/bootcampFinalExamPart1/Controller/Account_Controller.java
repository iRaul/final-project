package com.hk.novare.bootcampFinalExamPart1.Controller;

import com.hk.novare.bootcampFinalExamPart1.Service.Account_Service;
import com.hk.novare.bootcampFinalExamPart1.Model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("api/1.0.0/Account")
public class Account_Controller {

	@Autowired
	Account_Service _Account_Service_;

	@GetMapping(value = "/get/All")
	public ResponseEntity Get_Account()
	{
		return ResponseEntity.ok(this._Account_Service_.Get_Account());
	}

	@GetMapping(value = "/get/{id}")
	public Account Get_Account_By_Id(@PathVariable Long id)
	{
		return _Account_Service_.Get_Account_By_Id(id);
	}

	@PostMapping(value = "/Add")
	public ResponseEntity Add_Account(@RequestBody Map<String, Object> set_Data)
	{
		return ResponseEntity.ok(this._Account_Service_.Add_Account(set_Data));
	}

	@DeleteMapping("employee/delete/{id}")
	public void DeleteAccountById(@PathVariable(value="id") Long id)
	{
		this._Account_Service_.Delete_Account_Id(id);
	}
}

