package com.hk.novare.bootcampFinalExamPart1.Controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.hk.novare.bootcampFinalExamPart1.Model.Cart;
import com.hk.novare.bootcampFinalExamPart1.Service.Cart_Service;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("api/1.0.0/Cart")
public class Cart_Controller
{
	@Autowired
	private Cart_Service _Cart_Service_;

	@PostMapping(value = "Product/Add")
	public ResponseEntity Add_Product(@RequestBody Map<String, Object> set_Data)
	{
		return ResponseEntity.ok(this._Cart_Service_.Add_Product(set_Data));
	}

	@GetMapping(value = "User/{id}")
	public ResponseEntity Add_Product(@PathVariable("id") Long set_Id)
	{
		return ResponseEntity.ok(this._Cart_Service_.Get_By_User(set_Id));
	}

	@PostMapping(value = "User/Checkout")
	public ResponseEntity Cart_Checkout(@RequestBody Map<String, Object> set_Data)
	{
		return ResponseEntity.ok(this._Cart_Service_.Cart_Checkout(set_Data));
	}

	@PostMapping(value = "Product/Update")
	public ResponseEntity Product_Update(@RequestBody Map<String, Object> set_Data)
	{
		return ResponseEntity.ok(this._Cart_Service_.Product_Update(set_Data));
	}

	@PostMapping(value = "Product/Delete")
	public ResponseEntity Product_Delete(@RequestBody Map<String, Object> set_Data)
	{
		return ResponseEntity.ok(this._Cart_Service_.Product_Delete(set_Data));
	}
}	