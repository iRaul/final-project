package com.hk.novare.bootcampFinalExamPart1.Configuration;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.cors.CorsConfiguration;

@Configuration
@EnableWebSecurity
public class Main_Configuration extends WebSecurityConfigurerAdapter
{
	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http.cors().and().csrf().disable().authorizeRequests()
		.antMatchers(HttpMethod.GET,"/User").permitAll()
		.antMatchers(HttpMethod.POST,"/User").permitAll()
		.antMatchers(HttpMethod.GET,"/Product").permitAll()
		.antMatchers(HttpMethod.POST,"/Product").permitAll()
		.antMatchers(HttpMethod.GET,"/Cart").permitAll()
		.antMatchers(HttpMethod.POST,"/Cart").permitAll()
		.and()
		.httpBasic();

		//http.httpBasic().authenticationEntryPoint(authenticationEntryPoint);
		//http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	/*@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.setAllowedHeaders(List.of("Authorization", "Cache-Control", "Content-Type"));
		corsConfiguration.setAllowedOrigins(List.of("*"));
		corsConfiguration.setAllowedMethods(List.of("GET", "POST"));
		corsConfiguration.setAllowCredentials(true);
		corsConfiguration.setExposedHeaders(List.of("Authorization"));

		// You can customize the following part based on your project, it's only a sample
		http.authorizeRequests().antMatchers("/**").permitAll().anyRequest()
		.authenticated().and().csrf().disable().cors().configurationSource(request -> corsConfiguration);

	}*/
}