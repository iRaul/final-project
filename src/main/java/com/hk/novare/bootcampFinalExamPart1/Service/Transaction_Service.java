package com.hk.novare.bootcampFinalExamPart1.Service;

import org.springframework.stereotype.Service;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.HashMap;

import com.hk.novare.bootcampFinalExamPart1.Repository.Transaction_Repository;

import com.hk.novare.bootcampFinalExamPart1.Model.Transaction;

@Service
public class Transaction_Service
{
	private Transaction_Repository _Transaction_Repository_;

	public Transaction_Service(Transaction_Repository set_Transaction_Repository)
	{
		this._Transaction_Repository_ = set_Transaction_Repository;
	}
}