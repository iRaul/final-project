package com.hk.novare.bootcampFinalExamPart1.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.hk.novare.bootcampFinalExamPart1.Model.Account;
import com.hk.novare.bootcampFinalExamPart1.Model.Product;
import com.hk.novare.bootcampFinalExamPart1.Model.Token;

import com.hk.novare.bootcampFinalExamPart1.Repository.Token_Repository;

@Service
public class Token_Service
{

	@Autowired
	private Token_Repository _Token_Repository_;

	public Token_Service(Token_Repository set_Token_Repository) {

		this._Token_Repository_ = set_Token_Repository;
	}

	public List<Token> Get_Token()
	{
		return this._Token_Repository_.findAll();
	}

public Token Add_Token(HashMap<String, Object> set_Data) {

		Token _Token_ = new Token();

		_Token_.setTokenDescription(set_Data.get("tokenDescription").toString());
		_Token_.setStatus(set_Data.get("status").toString());

		ObjectMapper _ObjectMapper_ = new ObjectMapper();

		HashMap<String, Object> Data = _ObjectMapper_.convertValue(this._Token_Repository_.save(_Token_), HashMap.class);
		return this._Token_Repository_.save(_Token_);
	}

	public void Delete_Token_Id(Long id) { _Token_Repository_.deleteById(id);}

	public Optional<Token> Get_Token_By_Id(Long id) {return _Token_Repository_.findById(id);}

	public void Update_By_Id(Token token , Long id){

		Token token1 = _Token_Repository_.findById(id).orElse(null);

		_Token_Repository_.save(token1); }
}