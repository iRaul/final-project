package com.hk.novare.bootcampFinalExamPart1.Service;

import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hk.novare.bootcampFinalExamPart1.Mail.MailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;

import com.hk.novare.bootcampFinalExamPart1.Model.User;
import com.hk.novare.bootcampFinalExamPart1.Repository.User_Repository;

@Service
public class User_Service
{

	@Autowired
	private MailServiceImpl mailService;


	@Autowired
	private User_Repository _User_Repository_;

	public User Login(Map<String, Object> set_Data)
	{
		User _User_ = this._User_Repository_.findUserByUsernameAndPassword(set_Data.get("username").toString(), set_Data.get("password").toString());
		
		if (_User_ != null)
			return	_User_;

		else
			return null;
	}

	public String Logout(HttpServletRequest set_HttpServletRequest, HttpServletResponse set_HttpServletResponse)
	{
		Authentication _Authentication_ = SecurityContextHolder.getContext().getAuthentication();  

		if (_Authentication_ != null)
			new SecurityContextLogoutHandler().logout(set_HttpServletRequest, set_HttpServletResponse, _Authentication_);

		return "Success";
	}

	public User Insert(User set_User)
	{
		return this._User_Repository_.save(set_User);
	}

	public User Insert_Customer(User set_User)
	{
		set_User.setType("Customer");
		System.out.println("Email: " + set_User.getEmail());
		mailService.sendHtmlMessage(set_User.getEmail(), "Signup Successful", "<p>You have successfully registered at our website!</p><br> " +"<p>You have successfully registered as " + "<strong>" + set_User.getUsername() + "</strong>" + ".");
		return this.Insert(set_User);
	}
}