package com.hk.novare.bootcampFinalExamPart1.Service;

import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
//import java.util.stream;
import java.util.stream.Collectors;


import com.hk.novare.bootcampFinalExamPart1.Mail.MailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.http.ResponseEntity;

import java.time.Instant;

import com.hk.novare.bootcampFinalExamPart1.Model.Account;
import com.hk.novare.bootcampFinalExamPart1.Model.Cart;
import com.hk.novare.bootcampFinalExamPart1.Model.Order;
import com.hk.novare.bootcampFinalExamPart1.Model.Product;
import com.hk.novare.bootcampFinalExamPart1.Model.User;

import com.hk.novare.bootcampFinalExamPart1.Repository.Account_Repository;
import com.hk.novare.bootcampFinalExamPart1.Repository.Cart_Repository;
import com.hk.novare.bootcampFinalExamPart1.Repository.Order_Repository;
import com.hk.novare.bootcampFinalExamPart1.Repository.Product_Repository;
import com.hk.novare.bootcampFinalExamPart1.Repository.User_Repository;

@Service
public class Cart_Service
{

	@Autowired
	private MailServiceImpl mailService;

	@Autowired
	private Account_Repository _Account_Repository_;

	@Autowired
	private Cart_Repository _Cart_Repository_;

	@Autowired
	private Order_Repository _Order_Repository_;

	@Autowired
	private Product_Repository _Product_Repository_;

	@Autowired
	private User_Repository _User_Repository_;

	public Cart Insert(Cart set_Cart)
	{
		return this._Cart_Repository_.save(set_Cart);
	}

	public Map<String, Object> Add_Product(Map<String, Object> set_Data)
	{
		Map<String, Object> Response = new HashMap<String, Object>();

		Product _Product_ = this._Product_Repository_.findById(Long.parseLong(set_Data.get("product_id").toString())).orElse(null);
		User _User_ = this._User_Repository_.findById(Long.parseLong(set_Data.get("user_id").toString())).orElse(null);
		Order _Order_ = this._Order_Repository_.findByUserAndStatus(_User_, "Ordering");

		if (_Order_ == null)
		{
			_Order_ = new Order();
			_Order_.setUser(_User_);
			_Order_.setStatus("Ordering");
		}

		_Order_ = this._Order_Repository_.save(_Order_);

		Cart _Cart_ = new Cart();
		_Cart_.setOrderQuantity(Integer.parseInt(set_Data.get("order_quantity").toString()));
		_Cart_.setProduct(_Product_);
		_Cart_.setOrder(_Order_);

		Response.put("Status", "Success");
		Response.put("Data", this.Insert(_Cart_));

		return Response;
	}

	public Map<String, Object> Get_By_User(Long set_Id)
	{
		Map<String, Object> Response = new HashMap<String, Object>();

		User _User_ = this._User_Repository_.findById(set_Id).orElse(null);
		Order _Order_ = this._Order_Repository_.findByUserAndStatus(_User_, "Ordering");

		Set<Customer_Cart> _Customer_Cart_ = this._Cart_Repository_.findAllByOrder(_Order_, Customer_Cart.class);

		Map<String, Object> Response_2 = new HashMap<String, Object>();
		Response_2.put("Order", _Order_);
		Response_2.put("Cart", _Customer_Cart_);

		Response.put("Status", "Success");
		Response.put("Data", Response_2);

		return Response;
	}

	public interface Customer_Cart
	{
		public Long getId();
		public int getOrderQuantity();
		public Product getProduct();
	}

	public Map<String, Object> Cart_Checkout(Map<String, Object> set_Data)
	{
		Map<String, Object> Response = new HashMap<String, Object>();

		User _User_ = this._User_Repository_.findById(Long.parseLong(set_Data.get("user_id").toString())).orElse(null);
		
		Account _Account_;

		if (set_Data.containsKey("account_id"))
			_Account_ = this._Account_Repository_.findById(Long.parseLong(set_Data.get("account_id").toString())).orElse(null);

		else
			_Account_ = null;

		Order _Order_ = this._Order_Repository_.findByUserAndStatus(_User_, "Ordering");

		Response.put("Status", "Success");

		if (_Order_ == null)		
			Response.put("Message", "No Order to Checkout");

		else
		{
			_Order_.setStatus("Checkout");

			if (_Account_ != null)
				_Order_.setAccount(_Account_);

			else
				_Order_.setPaymentOption(set_Data.get("payment_option").toString());


			Set<Cart> _Cart_ = this._Cart_Repository_.findAllByOrder(_Order_);

			double qwe = 0;
			for (Cart set_Cart : _Cart_)
			{
				qwe += set_Cart.getProduct().getPrice() * set_Cart.getOrderQuantity();
			}

			_Order_.setTotal(qwe);
			_Order_.setOrderDate(Instant.now());
			
			Map<String, Object> Response_Data = new HashMap<String, Object>();
			
			Response_Data.put("Order", this._Order_Repository_.save(_Order_));
			Response_Data.put("Cart", this._Cart_Repository_.findAllByOrder(_Order_, Customer_Cart.class));
			Response.put("Data", Response_Data);

			mailService.sendHtmlMessage(_User_.getEmail(), "Transaction Success", "<p>You have successfully purchased at our website!");
		}

		return Response;
	}

	public Map<String, Object> Product_Update(Map<String, Object> set_Data)
	{
		Map<String, Object> Response = new HashMap<String, Object>();

		/*Product _Product_ = this._Product_Repository_.findById(Long.parseLong(set_Data.get("product_id").toString())).orElse(null);
		User _User_ = this._User_Repository_.findById(Long.parseLong(set_Data.get("user_id").toString())).orElse(null);
		Order _Order_ = this._Order_Repository_.findByUserAndStatus(_User_, "Ordering");
		*/
		Cart _Cart_ = _Cart_Repository_.findById(Long.valueOf(set_Data.get("cart_id").toString())).orElse(null);//_Cart_Repository_.findByProductIdAndOrderId(_Order_.getId(), _Product_.getId());
		_Cart_.setOrderQuantity(Integer.parseInt(set_Data.get("order_quantity").toString()));

		Response.put("Status", "Success");
		Response.put("Data", this._Cart_Repository_.save(_Cart_));

		return Response;
	}

	@Transactional
	public Map<String, Object> Product_Delete(Map<String, Object> set_Data)
	{
		//List<Integer> stringValues =  (List)set_Data_1.get("product_id");//new HashSet<Long>((List)set_Data_1.get("product_id"));

		Map<String, Object> Response = new HashMap<String, Object>();

		//this._Cart_Repository_.deleteByOrderIdAndProductIds(Long.valueOf(set_Data_1.get("order_id").toString()), stringValues);//Arrays.asList(set_Data_1.get("product_id")).stream().map(set_Data_2 -> Long.valueOf(set_Data_2.toString())).collect(Collectors.toSet()));

		//List<String> qwe = Arrays.asList(set_Data_1.get("product_id")).stream()
		//.map(potek -> potek + "asd")
		//.collect(Collectors.toList());

		//List<Object> qwe = (List<Integer>) Arrays.asList(set_Data_1.get("product_id"));
		
		//List<Long> hays = Arrays.asList(set_Data_1.get("product_id")).stream().map(s -> Long.parseLong(s.toString())).collect(Collectors.toList());
		
		//List<Number> hays = (List<Number>) set_Data.get("product_id");
		List<Number> hays = (List<Number>) set_Data.get("cart_id");

		if (hays.size() != 1)
		{
			Response.put("lahh", "lahh");
			System.out.println("lahh");
			List<Long> qwe = new ArrayList<Long>();
			//for (int x = 0; hays.)
			for (Number wtf : hays)
			{
				qwe.add(Long.parseLong(wtf.toString()));
				System.out.println(wtf.toString());
			}

			this._Cart_Repository_.deleteByIdIn(qwe);
		}

		else
		{
			System.out.println("hehh");
			Response.put("hehh", "hehh");
			this._Cart_Repository_.deleteById(Long.parseLong(hays.get(0).toString()));
		}


		//this._Cart_Repository_.deleteAllByOrderIdAndProductIdIn(Long.valueOf(set_Data_1.get("order_id").toString()), qwe);

		//while(hays.hasNext())
		//{

		//}
		//for (long wtf : hays)
		//{
		//	System.out.println("wtf".toString());
		//};

		//System.out.println(hays.getClass().toString());

		//for (Long zz : qwe)
		//{
		//	Response.put(zz.toString(), zz);
			//Cart zz = this._Cart_Repository_.findByProductIdAndOrderId(Long.valueOf(set_Data_2.toString()), Long.valueOf(set_Data_1.get("order_id").toString()));
			//this._Cart_Repository_.deleteByCart(zz);
		//}

		//this._Cart_Repository_.deleteAllByOrderIdAndProductIdIn(Long.valueOf(set_Data.get("order_id").toString()), qwe);
		
		//em.remove(employee);
		//em.getTransaction().commit();
		Response.put("Status", "Success");//Arrays.asList(set_Data_1.get("product_id")).stream().map(set_Data_2 -> set_Data_2.toString()).collect(Collectors.toSet()));
		Response.put("ZZ", hays.size());
		return Response;
	}
}