package com.hk.novare.bootcampFinalExamPart1.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.PageRequest;

import org.springframework.stereotype.Service;
import org.springframework.http.ResponseEntity;

import com.hk.novare.bootcampFinalExamPart1.Repository.Product_Repository;
import com.hk.novare.bootcampFinalExamPart1.Model.Product;

@Service
public class Product_Service
{
	@Autowired
	private Product_Repository _Product_Repository_;

	public Set<Product> paginggg()
	{
		//int pageNumber = 1;
		//int pageSize = 10;
		//Sort sort = Sort.by(Sort.Direction.ASC, "id");
		//Pageable pageable = PageRequest.of(0, 10, sort);
		//Page<Product> page = this._Product_Repository_.findAll(pageable);
		return new HashSet<Product>(this._Product_Repository_.findAll(PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"))).getContent());
	}

	public Set<Product> Select()
	{
		return new HashSet<Product>(this._Product_Repository_.findAll());
	}

	public Set<Product> Select(String set_Action, Integer set_PageNumber)
	{
		if (set_Action.equals("All"))
			return new HashSet<Product>(this._Product_Repository_.findAll(PageRequest.of(set_PageNumber.intValue() - 1, 10, Sort.by(Sort.Direction.ASC, "id"))).getContent());
		
		else
			return null;
	}

	public Set<Product> Select(String set_Action, String set_Data, Integer set_PageNumber)
	{
		if (set_Action.equals("Category"))
			return new HashSet<Product>(this._Product_Repository_.findAllByCategory(set_Data, PageRequest.of(set_PageNumber.intValue() - 1, 10, Sort.by(Sort.Direction.ASC, "category"))).getContent());

		else
			return null;
	}

	public Product Insert(Product set_Product)
	{
		return this._Product_Repository_.save(set_Product);
	}

	public Product Update(Product set_Product)
	{
		return this._Product_Repository_.save(set_Product);
	}

	public Map<String, Object> Delete(Long set_Id)
	{
		Map<String, Object> Response = new HashMap<String, Object>();
		this._Product_Repository_.deleteById(set_Id);
		Response.put("Status" , "Success");
		return Response;
	}

	/*public Product qwe(HashMap<String, Object> set_Data)
	{
		/*Product _Product_ = new Product();

		_Product_.setProductName(set_Data.get("productName").toString());
		_Product_.setCategory(set_Data.get("category").toString());
		_Product_.setProductDescription(set_Data.get("productDescription").toString());
		_Product_.setProductQuantity(Integer.parseInt(set_Data.get("productQuantity").toString()));
		_Product_.setPrice(Double.parseDouble(set_Data.get("price").toString()));

		//ObjectMapper _ObjectMapper_ = new ObjectMapper();

		//HashMap<String, Object> Data = _ObjectMapper_.convertValue(this._Product_Repository_.save(_Product_), HashMap.class)
		return this._Product_Repository_.save(_Product_);
	}*/

	public Product Get_Specific(Long set_Id)
	{
		return this._Product_Repository_.findOneById(set_Id);
	}

	public Set<Product> Get_All()
	{
		return this.Select();
	}

	public Set<Product> Get_All(String set_Action, Integer set_PageNumber)
	{
		return this.Select(set_Action, set_PageNumber);
	}

	public Set<Product> Search_Product(String productName){
		return this._Product_Repository_.findByProductNameContaining(productName);
	}

	public Set<Product> Get_All_By_Category(String set_Action, String set_Data, Integer set_PageNumber)
	{
		return this.Select(set_Action, set_Data, set_PageNumber);
	}

	public Product Add_Product(Product set_Product)
	{
		return this.Insert(set_Product);
	}

	public Map<String, Object> Delete_Product(Long set_Id)
	{
		return this.Delete(set_Id);
	}
}