package com.hk.novare.bootcampFinalExamPart1.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.convert.Jsr310Converters;
import org.springframework.stereotype.Service;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.node.ObjectNode;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.hk.novare.bootcampFinalExamPart1.Repository.Account_Repository;
import com.hk.novare.bootcampFinalExamPart1.Repository.User_Repository;

import com.hk.novare.bootcampFinalExamPart1.Model.Account;
import com.hk.novare.bootcampFinalExamPart1.Model.User;

@Service
public class Account_Service
{
	@Autowired
	private Account_Repository _Account_Repository_;

	@Autowired
	private User_Repository _User_Repository_;

	public List<Account> Get_Account() { return this._Account_Repository_.findAll(); }

	public Account Add_Account(Map<String, Object> set_Data)
	{
		User _User_ = this._User_Repository_.findById(Long.parseLong(set_Data.get("user_id").toString())).orElse(null);
		Account _Account_ = new Account();

		_Account_.setAccountName(set_Data.get("accountName").toString());
		_Account_.setAccountNumber(set_Data.get("accountNumber").toString());
		_Account_.setAccountType(set_Data.get("accountType").toString());
		_Account_.setBalance(Double.parseDouble(set_Data.get("balance").toString()));
		_Account_.setExpirationDate(Instant.parse(set_Data.get("expirationDate").toString()));
		_Account_.setUser(_User_);

		return this._Account_Repository_.save(_Account_);
	}

	public void Delete_Account_Id(Long id) { _Account_Repository_.deleteById(id);}

	public Account Get_Account_By_Id(Long id)
	{
		return this._Account_Repository_.findById(id).orElse(null);
	}

//	public void DeleteById(Long id) { _Account_Repository_.DeleteAccountById(id);}
//
//	public List<Account> Get_Account_By_Id(Long id) {return this._Account_Repository_.GetAccountById(id);}



}